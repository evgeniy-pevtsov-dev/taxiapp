package helpers

//APIRequest api request structure
type APIRequest struct {
	Token            string  `json:"token" form:"token" binding:"required"`
	Command          string  `json:"command" form:"command" binding:"required"`
	Phone            string  `json:"phone" form:"phone"`
	DestinationPhone string  `json:"phone2" form:"phone2"`
	Name             string  `json:"name" form:"name"`
	StateNumber      string  `json:"state_number" form:"state_number"`
	Color            string  `json:"color" form:"color"`
	Brand            string  `json:"brand" form:"brand"`
	Code1            string  `json:"code_1" form:"code_1"`
	Code2            string  `json:"code_2" form:"code_2"`
	Lat              float64 `json:"lat" form:"lat"`
	Lng              float64 `json:"lng" form:"lng"`
	Screen           string  `json:"screen" form:"screen"`
	City             string  `json:"city" form:"city"`
	Doc              string  `json:"doc" form:"doc"`
	Address          string  `json:"address" form:"address"`
	Coordinates      string  `json:"coordinates" form:"coordinates"`
	Image            string  `json:"image" form:"image"`
}

//UploadedFile struct for file uploading
type UploadedFile struct {
	OriginalName string
	UploadedName string
	Error        string
}
