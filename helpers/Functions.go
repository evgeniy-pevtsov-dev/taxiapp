package helpers

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

//GetEnv get environment variable
func GetEnv(key string) string {

	value, exists := os.LookupEnv(key)

	if exists {
		return value
	}

	return ""

}

//ClearPhone remove all non-numeric chars from phone number
func ClearPhone(phone string) string {

	reg, err := regexp.Compile("[^0-9]+")
	if err != nil {
		log.Fatal(err)
	}
	result := reg.ReplaceAllString(phone, "")

	return result

}

//UniqID generate uinque id
func UniqID(prefix string) string {
	now := time.Now()
	sec := now.Unix()
	usec := now.UnixNano() % 0x100000
	return fmt.Sprintf("%s%08x%05x", prefix, sec, usec)
}

//UploadFile upload file
func UploadFile(c *gin.Context, fileName string, userDir string) (result []UploadedFile) {

	dir := GetEnv("DOCS_DIR")

	if _, err := os.Stat(dir + "/" + userDir); os.IsNotExist(err) {
		os.Mkdir(dir+"/"+userDir, 0755)
	}

	formdata, _ := c.MultipartForm()
	files := formdata.File[fileName]

	for i := range files {
		file := files[i]
		filename := strings.Split(file.Filename, ".")

		storeFilename := UniqID("") + "." + filename[len(filename)-1]
		storeFilename = dir + "/" + userDir + "/" + storeFilename

		err := c.SaveUploadedFile(file, storeFilename)

		if err != nil {
			log.Println("Docs: " + err.Error())
			result = append(result, UploadedFile{
				OriginalName: file.Filename,
				Error:        err.Error(),
			})
		} else {
			result = append(result, UploadedFile{
				OriginalName: file.Filename,
				UploadedName: storeFilename,
			})
		}

	}

	return
}

func Base64ToImage(image string, userDir string) (result []UploadedFile) {

	dir := GetEnv("DOCS_DIR")

	if _, err := os.Stat(dir + "/" + userDir); os.IsNotExist(err) {
		os.Mkdir(dir+"/"+userDir, 0755)
	}

	coI := strings.Index(string(image), ",")
	rawImage := string(image)[coI+1:]
	unbased, _ := base64.StdEncoding.DecodeString(string(rawImage))

	res := bytes.NewReader(unbased)

	if coI == -1 {
		/*jpgI, err := jpeg.Decode(res)
		if err != nil {
			log.Println("Base64ToImag: " + err.Error())
			return
		}*/
		storeFilename := userDir + "/" + UniqID("_") + ".jpg"
		err := ioutil.WriteFile(dir+"/"+storeFilename, unbased, 0644)
		if err != nil {
			log.Println("Base64ToImag: " + err.Error())
			return
		}
		/*
			f, _ := os.OpenFile(dir+"/"+storeFilename, os.O_WRONLY|os.O_CREATE, 0777)
			jpeg.Encode(f, jpgI, &jpeg.Options{Quality: 100})
		*/
		result = append(result, UploadedFile{
			OriginalName: "",
			UploadedName: storeFilename,
		})

	} else {
		switch strings.TrimSuffix(image[5:coI], ";base64") {
		case "image/png":
			pngI, err := png.Decode(res)

			if err != nil {
				log.Println("Base64ToImag: " + err.Error())
				return
			}
			storeFilename := userDir + "/" + UniqID("_") + ".png"
			f, _ := os.OpenFile(dir+"/"+storeFilename, os.O_WRONLY|os.O_CREATE, 0777)
			png.Encode(f, pngI)
			result = append(result, UploadedFile{
				OriginalName: "",
				UploadedName: storeFilename,
			})

			break
		case "image/jpeg":
			jpgI, err := jpeg.Decode(res)
			if err != nil {
				log.Println("Base64ToImag: " + err.Error())
				return
			}
			storeFilename := userDir + "/" + UniqID("_") + ".jpg"
			f, _ := os.OpenFile(dir+"/"+storeFilename, os.O_WRONLY|os.O_CREATE, 0777)
			jpeg.Encode(f, jpgI, &jpeg.Options{Quality: 100})
			result = append(result, UploadedFile{
				OriginalName: "",
				UploadedName: storeFilename,
			})

			break
		}
	}

	return
}

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func RandomString(length int) string {
	return StringWithCharset(length, charset)
}
