package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/evgeniy-pevtsov-dev/taxiapp/controllers"
	"bitbucket.org/evgeniy-pevtsov-dev/taxiapp/helpers"
	"bitbucket.org/evgeniy-pevtsov-dev/taxiapp/websocket"
	"github.com/gin-gonic/gin"
)

func serveWs(pool *websocket.Pool, w http.ResponseWriter, r *http.Request) {
	fmt.Println("WebSocket Endpoint Hit")
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
	}

	phone := r.URL.Query().Get("phone")

	if phone == "" {
		return
	}

	phone = helpers.ClearPhone(phone)

	client := &websocket.Client{
		Conn: conn,
		Pool: pool,
		ID:   phone,
	}

	pool.Register <- client
	client.Read()
}

func init() {

}

func main() {

	gin.SetMode(gin.DebugMode)

	server := gin.Default()

	websocket.SocketPool = websocket.NewPool()
	go websocket.SocketPool.Start()

	logOutput, err := os.OpenFile("./output.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Panicf("Error opening log file: %v", err)
	}
	log.SetOutput(logOutput)

	server.Use(gin.RecoveryWithWriter(log.Writer()))
	server.Use(gin.LoggerWithConfig(gin.LoggerConfig{
		Output: log.Writer(),
	}))

	server.GET("/ping", controllers.Ping)
	server.POST("/ping", controllers.Ping)

	server.POST("/api", controllers.API)

	server.GET("/cron", controllers.Cron)

	server.GET("/ws", func(c *gin.Context) {
		serveWs(websocket.SocketPool, c.Writer, c.Request)
	})

	server.Run(":7575")
}
