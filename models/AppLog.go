package models

import (
	"encoding/json"
	"log"
	"time"

	"bitbucket.org/evgeniy-pevtsov-dev/taxiapp/helpers"
)

//AppLog coordinates struct
type AppLog struct {
	ID        uint64    `gorm:"Column:id;PRIMARY_KEY"`
	Command   string    `gorm:"Column:command"`
	Data      string    `gorm:"Column:data"`
	CreatedAt time.Time `gorm:"Column:created_at"`
	UpdatedAt time.Time `gorm:"Column:updated_at"`
}

//TableName define table name
func (AppLog) TableName() string {
	return "app_log"
}

//AddAppLog add app log
func AddAppLog(request helpers.APIRequest) {

	json, err := json.Marshal(request)

	if err != nil {
		log.Print(err.Error())
		return
	}

	appLog := AppLog{
		Command: request.Command,
		Data:    string(json),
	}

	DB.Create(&appLog)
}

//ClearLog clear all log data
func ClearLog() {

	result := DB.Exec("CALL `sp_clear_log`();")

	if result.Error != nil {
		log.Println("Cron: " + result.Error.Error())
	}
}
