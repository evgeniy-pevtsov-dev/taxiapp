package models

import (
	"strconv"
	"time"

	"bitbucket.org/evgeniy-pevtsov-dev/taxiapp/helpers"
)

//AppCoordinates coordinates struct
type AppCoordinates struct {
	ID        uint64    `gorm:"Column:id;PRIMARY_KEY"`
	Lat       float64   `gorm:"Column:lat"`
	Lng       float64   `gorm:"Column:lng"`
	CreatedAt time.Time `gorm:"Column:created_at"`
	UpdatedAt time.Time `gorm:"Column:updated_at"`
}

//TableName define table name
func (AppCoordinates) TableName() string {
	return "app_coordinates"
}

func AddCoordinates(request helpers.APIRequest) error {

	var err error

	var phoneDB uint64

	phone := helpers.ClearPhone(request.Phone)

	if phoneDB, err = strconv.ParseUint(phone, 10, 64); err != nil {
		return err
	}

	err = SetCoordinates(phoneDB, request.Lat, request.Lng)

	return err
}

//SetCoordinates set coordinates
func SetCoordinates(phone uint64, lat float64, lng float64) error {

	var err error

	data := AppCoordinates{
		ID:        phone,
		Lat:       lat,
		Lng:       lng,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	result := DB.Save(data)

	if result.RowsAffected == 0 {
		err = DB.Create(&data).Error
	}

	return err
}
