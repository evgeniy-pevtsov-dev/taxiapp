package models

import (
	"log"
	"strings"
	"time"
)

//AppCityInfo districts struct
type AppCityInfo struct {
	ID        uint64    `gorm:"Column:id;PRIMARY_KEY"`
	CityID    uint32    `gorm:"Column:city_id"`
	Info1     string    `gorm:"Column:info1"`
	Info2     string    `gorm:"Column:info2"`
	Info3     string    `gorm:"Column:info3"`
	Info4     string    `gorm:"Column:info4"`
	CreatedAt time.Time `gorm:"Column:created_at"`
	UpdatedAt time.Time `gorm:"Column:updated_at"`
}

//GetInfoCityResult info city result
type GetInfoCityResult struct {
	Info1 []string
	Info2 []string
	Info3 []string
	Info4 []string
}

//GetDistrictsByCity get districts by city
func GetInfoByCity(city string) (info GetInfoCityResult, err error) {

	var data []*AppCityInfo

	result := DB.Raw("CALL `sp_get_info`(?);", city).Scan(&data)

	if result.Error != nil {
		err = result.Error
		log.Println(err.Error())
		return
	}

	for _, row := range data {
		info.Info1 = strings.Split(strings.ReplaceAll(row.Info1, "\r", ""), "\n")
		info.Info2 = strings.Split(strings.ReplaceAll(row.Info2, "\r", ""), "\n")
		info.Info3 = strings.Split(strings.ReplaceAll(row.Info3, "\r", ""), "\n")
		info.Info4 = strings.Split(strings.ReplaceAll(row.Info4, "\r", ""), "\n")
	}

	return

}
