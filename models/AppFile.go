package models

import "time"

//AppFile files struct
type AppFile struct {
	ID        uint64    `gorm:"Column:id;PRIMARY_KEY"`
	DataID    uint32    `gorm:"Column:data_id"`
	FileName  string    `gorm:"Column:filename"`
	CreatedAt time.Time `gorm:"Column:created_at"`
	UpdatedAt time.Time `gorm:"Column:updated_at"`
}

func (AppFile) TableName() string {
	return "app_files"
}

//AddFile add new file
func AddFile(dataID uint32, filename string) (data AppFile) {
	data.CreatedAt = time.Now()
	data.DataID = dataID
	data.FileName = filename

	DB.Create(&data)

	return
}

//DeleteFilebyDataID delete files by DataID
func DeleteFilebyDataID(dataID uint32) {

	DB.Where("data_id = ?", dataID).Delete(&AppFile{})

}
