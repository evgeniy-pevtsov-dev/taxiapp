package models

import "time"

//AppCodeHistory code history struct
type AppCodeHistory struct {
	ID        uint64    `gorm:"Column:id;AUTO_INCREMENT;PRIMARY_KEY"`
	Phone     uint64    `gorm:"Column:phone"`
	Code1     string    `gorm:"Column:code_1"`
	Code2     string    `gorm:"Column:code_2"`
	CreatedAt time.Time `gorm:"Column:created_at"`
	UpdatedAt time.Time `gorm:"Column:updated_at"`
}

//TableName define table name
func (AppCodeHistory) TableName() string {
	return "app_code_history"
}
