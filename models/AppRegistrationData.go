package models

import (
	"log"
	"strconv"
	"time"

	"bitbucket.org/evgeniy-pevtsov-dev/taxiapp/helpers"
	"github.com/jinzhu/gorm"
)

//AppRegistrationData registration data struct
type AppRegistrationData struct {
	ID          uint32    `gorm:"Column:id;AUTO_INCREMENT;PRIMARY_KEY"`
	Active      uint      `gorm:"Column:active"`
	CodeID      uint32    `gorm:"Column:code_id"`
	Name        string    `gorm:"Column:name"`
	Phone       uint64    `gorm:"Column:phone"`
	StateNumber string    `gorm:"Column:state_number"`
	Color       string    `gorm:"Column:color"`
	Brand       string    `gorm:"Column:brand"`
	Status      int       `gorm:"Column:status"`
	Code1       string    `gorm:"Column:code_1"`
	Code2       string    `gorm:"Column:code_2"`
	Token       string    `gorm:"Column:token"`
	CreatedAt   time.Time `gorm:"Column:created_at"`
	UpdatedAt   time.Time `gorm:"Column:updated_at"`
}

//TableName define table name
func (AppRegistrationData) TableName() string {
	return "app_registration_data"
}

//GetRegistrationData get registration data by phone
func GetRegistrationData(phone uint64) (AppRegistrationData, error) {

	var data AppRegistrationData
	var err error

	result := DB.Where("phone = ?", phone).First(&data)

	if result.Error != nil {
		err = result.Error
	}
	if err != nil {
		log.Println(err.Error())
	}

	return data, err

}

//AddRegistrationData create new registration
func AddRegistrationData(request helpers.APIRequest) (AppRegistrationCode, uint32, string, error) {

	var err error
	var code AppRegistrationCode
	var dataFind AppRegistrationData
	var phoneDB uint64

	token := helpers.RandomString(32)

	if phoneDB, err = strconv.ParseUint(request.Phone, 10, 64); err != nil {
		return code, 0, token, err
	}

	result := DB.
		Where("phone = ?", phoneDB).
		First(&dataFind)

	if result.Error != nil && !gorm.IsRecordNotFoundError(result.Error) {
		err = result.Error
		log.Println(err.Error())
		return code, 0, "", err
	}

	if dataFind.ID > 0 {
		if dataFind.Token == request.Token {
			DB.Where("id = ?", dataFind.Phone).Delete(AppCoordinates{})
			DB.Delete(&dataFind)
		}

		return code, 0, "", nil
	}

	data := AppRegistrationData{
		Active:      1,
		Name:        request.Name,
		Phone:       phoneDB,
		StateNumber: request.StateNumber,
		Color:       request.Color,
		Brand:       request.Brand,
		Token:       token,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	result = DB.Create(&data)

	if result.Error != nil {
		err = result.Error
		log.Println(err.Error())
		return code, 0, token, err
	}

	code, err = GetFreeRegistrationCode()

	if err != nil {
		log.Println(err.Error())
		return code, data.ID, token, err
	}

	data.CodeID = code.ID

	result = DB.Model(&data).Update("code_id", data.CodeID)

	if result.Error != nil {
		err = result.Error
		log.Println(err.Error())
		return code, data.ID, token, err
	}

	return code, data.ID, token, nil
}

//UpdateRegistrationData update registration data
func UpdateRegistrationData(request helpers.APIRequest) (dataFind AppRegistrationData, dataID uint32, err error) {

	var phoneDB uint64

	if phoneDB, err = strconv.ParseUint(request.Phone, 10, 64); err != nil {
		return
	}

	result := DB.Where("phone = ?", phoneDB).Where("token = ?", request.Token).First(&dataFind)
	if result.Error != nil && !gorm.IsRecordNotFoundError(result.Error) {
		err = result.Error
		log.Println(err.Error())
		return
	}

	if dataFind.ID > 0 {

		dataID = dataFind.ID

		dataFind.Name = request.Name
		dataFind.StateNumber = request.StateNumber
		dataFind.Color = request.Color
		dataFind.Brand = request.Brand
		dataFind.UpdatedAt = time.Now()

		result = DB.Save(&dataFind)

		if result.Error != nil {
			err = result.Error
		}

	}

	return

}

//AddCodeHistory add code history
func AddCodeHistory(phone uint64, code1 string, code2 string) error {

	var err error

	var data = AppCodeHistory{
		Phone:     phone,
		Code1:     code1,
		Code2:     code2,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	result := DB.Create(&data)

	if result.Error != nil {
		err = result.Error
	}

	return err

}

//SetRegistrationDataCode1 set code_1
func SetRegistrationDataCode1(request helpers.APIRequest) error {

	var err error
	var data AppRegistrationData
	var phoneDB uint64

	if phoneDB, err = strconv.ParseUint(request.Phone, 10, 64); err != nil {
		return err
	}

	result := DB.Model(&data).Where("phone = ?", phoneDB).Update("code_1", request.Code1).Update("updated_at", time.Now())

	if result.RowsAffected > 0 {
		err = AddCodeHistory(phoneDB, request.Code1, "")
	}

	return err
}

//SetRegistrationDataCode2 set code_2
func SetRegistrationDataCode2(request helpers.APIRequest) error {

	var err error
	var data AppRegistrationData
	var phoneDB uint64

	if phoneDB, err = strconv.ParseUint(request.Phone, 10, 64); err != nil {
		return err
	}

	result := DB.Model(&data).Where("phone = ?", phoneDB).Update("code_2", request.Code2).Update("updated_at", time.Now())

	if result.RowsAffected > 0 {
		err = AddCodeHistory(phoneDB, "", request.Code2)
	}

	return err
}

//SetStatus set current status
func SetStatus(request helpers.APIRequest, status int) error {

	var err error
	var data AppRegistrationData
	var phoneDB uint64

	if phoneDB, err = strconv.ParseUint(request.Phone, 10, 64); err != nil {
		return err
	}

	result := DB.Model(&data).Where("phone = ?", phoneDB).Update("status", status)

	if result.Error != nil {
		err = result.Error
		log.Println(err.Error())
	} else {

		if status == 1 {
			SetCoordinates(phoneDB, request.Lat, request.Lng)
		}

	}

	return err

}

//GetFreeCountStruct free count result
type GetFreeCountStruct struct {
	Count int32 `gorm:"Column:free_count"`
}

//GetFreeCount get free by coord
func GetFreeCount(lat float64, lng float64) (data GetFreeCountStruct) {

	latMin := lat - 0.2
	latMax := lat + 0.2

	lngMin := lng - 0.2
	lngMax := lng + 0.2

	result := DB.Raw("CALL `sp_get_free`(?,?,?,?);", latMin, latMax, lngMin, lngMax).Scan(&data)

	if result.Error != nil {
		err := result.Error
		log.Println(err.Error())

	}
	return
}

//AppStatusCode status code struct
type AppStatusCode struct {
	Code1 string             `gorm:"Column:code_1"`
	Code2 string             `gorm:"Column:code_2"`
	Free  GetFreeCountStruct `gorm:"-"`
}

//GetCodes get last code
func GetCodes(request helpers.APIRequest) (AppStatusCode, error) {

	var data AppRegistrationData
	var err error
	var code AppStatusCode
	var phoneDB uint64

	if request.Phone != "" {
		phoneDB, err = strconv.ParseUint(request.Phone, 10, 64)

		if err == nil {
			data, err = GetRegistrationData(phoneDB)
			if err == nil {
				code.Code1 = data.Code1
				code.Code2 = data.Code2

			}
		}

		if err != nil {
			log.Println(err.Error())
		}
	}

	code.Free = GetFreeCount(request.Lat, request.Lng)

	return code, err
}

//GetFreeStruct free data struct
type GetFreeStruct struct {
	*AppRegistrationData
	Lat              float64 `gorm:"Column:lat"`
	Lng              float64 `gorm:"Column:lng"`
	RegistrationCode string  `gorm:"Column:code"`
}

//GetFree get free data
func GetFree(request helpers.APIRequest) (data []GetFreeStruct, err error) {
	latMin := request.Lat - 0.2
	latMax := request.Lat + 0.2

	lngMin := request.Lng - 0.2
	lngMax := request.Lng + 0.2

	result := DB.Raw("CALL `sp_get_free_data`(?,?,?,?);", latMin, latMax, lngMin, lngMax).Scan(&data)

	if result.Error != nil {
		err = result.Error
		log.Println(err.Error())

	}

	return
}
