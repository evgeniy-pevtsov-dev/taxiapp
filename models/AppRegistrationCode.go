package models

import (
	"log"
	"time"
)

//AppRegistrationCode registration code struct
type AppRegistrationCode struct {
	ID        uint32    `gorm:"Column:id;AUTO_INCREMENT;PRIMARY_KEY"`
	Code      string    `gorm:"Column:code"`
	CreatedAt time.Time `gorm:"Column:created_at"`
	UpdatedAt time.Time `gorm:"Column:updated_at"`
}

//TableName define table name
func (AppRegistrationCode) TableName() string {
	return "app_registration_codes"
}

//GetFreeRegistrationCode get last free registration code
func GetFreeRegistrationCode() (AppRegistrationCode, error) {

	var data AppRegistrationCode
	result := DB.Raw("CALL `sp_get_new_code`();").Scan(&data)

	if result.Error != nil {
		err := result.Error
		return data, err
	}

	return data, nil

}

//ClearExpiredCodes clear expired codes
func ClearExpiredCodes() {

	result := DB.Exec("CALL `sp_truncate_codes`();")

	if result.Error != nil {
		log.Println("Cron: " + result.Error.Error())
	}
}
