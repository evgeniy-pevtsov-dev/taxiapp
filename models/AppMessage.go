package models

import (
	"strconv"
	"strings"
	"time"

	"bitbucket.org/evgeniy-pevtsov-dev/taxiapp/helpers"
	"bitbucket.org/evgeniy-pevtsov-dev/taxiapp/websocket"
)

//AppMessage message struct
type AppMessage struct {
	ID          uint64    `gorm:"Column:id;PRIMARY_KEY"`
	PhoneFrom   uint64    `gorm:"Column:phone_from"`
	PhoneTo     uint64    `gorm:"Column:phone_to"`
	Message     string    `gorm:"Column:message"`
	Address     string    `gorm:"Column:address"`
	Coordinates string    `gorm:"Column:coordinates"`
	CreatedAt   time.Time `gorm:"Column:created_at"`
	UpdatedAt   time.Time `gorm:"Column:updated_at"`
}

//SendMessage send message
func SendMessage(request helpers.APIRequest) (err error) {

	var phoneDB uint64
	var phone2DB uint64

	if phoneDB, err = strconv.ParseUint(request.Phone, 10, 64); err != nil {
		return
	}

	if phone2DB, err = strconv.ParseUint(request.DestinationPhone, 10, 64); err != nil {
		return
	}

	data := AppMessage{
		PhoneFrom:   phoneDB,
		PhoneTo:     phone2DB,
		Message:     request.Screen,
		Address:     request.Address,
		Coordinates: request.Coordinates,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	err = DB.Create(&data).Error

	if err == nil {
		for client := range websocket.SocketPool.Clients {
			if strings.Compare(request.DestinationPhone, client.ID) == 0 {
				client.Conn.WriteJSON(websocket.Message{Type: 1, Phone: request.Phone, Screen: request.Screen, Message: "", Address: request.Address, Coordinates: request.Coordinates})
			}
		}
	}

	return
}
