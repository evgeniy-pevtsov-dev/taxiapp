package models

import "time"

//AppCity cities struct
type AppCity struct {
	ID        uint64    `gorm:"Column:id;PRIMARY_KEY"`
	RegionID  uint32    `gorm:"Column:region_id"`
	Name      string    `gorm:"Column:name"`
	Lat       float64   `gorm:"Column:lat"`
	Lng       float64   `gorm:"Column:lng"`
	CreatedAt time.Time `gorm:"Column:created_at"`
	UpdatedAt time.Time `gorm:"Column:updated_at"`
}
