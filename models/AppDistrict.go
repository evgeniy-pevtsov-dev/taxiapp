package models

import (
	"log"
	"time"
)

//AppDistrict districts struct
type AppDistrict struct {
	ID        uint64    `gorm:"Column:id;PRIMARY_KEY"`
	CityID    uint32    `gorm:"Column:city_id"`
	Name      string    `gorm:"Column:name"`
	CreatedAt time.Time `gorm:"Column:created_at"`
	UpdatedAt time.Time `gorm:"Column:updated_at"`
}

//GetDistrictsByCity get districts by city
func GetDistrictsByCity(city string) (data []*AppDistrict, err error) {

	result := DB.Raw("CALL `sp_get_districts`(?);", city).Scan(&data)

	if result.Error != nil {
		err = result.Error
		log.Println(err.Error())

	}

	return

}
