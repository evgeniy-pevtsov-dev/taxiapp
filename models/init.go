package models

import (
	"log"
	"os"
	"time"

	"bitbucket.org/evgeniy-pevtsov-dev/taxiapp/helpers"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/joho/godotenv"
)

//DB db link
var DB *gorm.DB

func init() {

	var err error

	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}

	DB, err = gorm.Open("mysql", helpers.GetEnv("DB_USERNAME")+":"+helpers.GetEnv("DB_PASSWORD")+"@/"+helpers.GetEnv("DB_DATABASE")+"?&parseTime=True")

	DB.SetLogger(log.New(os.Stdout, "\r\n", 0))
	DB.LogMode(true)

	//defer DB.Close()

	if err != nil {
		log.Panic("DB: " + err.Error())
	}

}

//ClearExpired clear expired data
func ClearExpired() {

	DB, _ := gorm.Open("mysql", helpers.GetEnv("DB_USERNAME")+":"+helpers.GetEnv("DB_PASSWORD")+"@/"+helpers.GetEnv("DB_DATABASE")+"?&parseTime=True")
	result := DB.Raw("CALL `sp_truncate_codes`();")

	if result.Error != nil && result.Error != gorm.ErrRecordNotFound {
		log.Println("ClearExpired: " + result.Error.Error())
	}

}

//CheckUpdateLocation check update location
func CheckUpdateLocation() {
	DB, _ := gorm.Open("mysql", helpers.GetEnv("DB_USERNAME")+":"+helpers.GetEnv("DB_PASSWORD")+"@/"+helpers.GetEnv("DB_DATABASE")+"?&parseTime=True")

	for range time.Tick(time.Minute * 1) {
		result := DB.Raw("CALL `sp_fix_status`();")
		if result.Error != nil && result.Error != gorm.ErrRecordNotFound {
			log.Println("CheckUpdateLocation: " + result.Error.Error())
		}
	}
}

func ClearAPPLog() {

	DB, _ := gorm.Open("mysql", helpers.GetEnv("DB_USERNAME")+":"+helpers.GetEnv("DB_PASSWORD")+"@/"+helpers.GetEnv("DB_DATABASE")+"?&parseTime=True")

	result := DB.Exec("CALL `sp_clear_log`();")

	if result.Error != nil {
		log.Println("Cron: " + result.Error.Error())
	}

}
