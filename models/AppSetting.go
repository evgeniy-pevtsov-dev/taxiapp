package models

import (
	"log"
)

//AppSetting settings struct
type AppSetting struct {
	ID    uint64 `gorm:"Column:id;PRIMARY_KEY"`
	Key   string `gorm:"Column:key"`
	Value string `gorm:"Column:value"`
}

func (AppSetting) TableName() string {
	return "settings"
}

//GetSetting get setting by name
func GetSetting(key string) (setting AppSetting, err error) {

	result := DB.Where("`key` = ?", key).First(&setting)

	if result.Error != nil {
		err = result.Error
		log.Println(err.Error())
	}

	return
}
