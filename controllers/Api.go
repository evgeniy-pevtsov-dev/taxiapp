package controllers

import (
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/evgeniy-pevtsov-dev/taxiapp/helpers"
	"bitbucket.org/evgeniy-pevtsov-dev/taxiapp/models"
	"github.com/gin-gonic/gin"
)

//API main api handler
func API(c *gin.Context) {
	var request helpers.APIRequest
	if err := c.Bind(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	go func() {
		models.AddAppLog(request)
	}()

	request.Phone = helpers.ClearPhone(request.Phone)
	request.DestinationPhone = helpers.ClearPhone(request.DestinationPhone)

	switch request.Command {

	case "1":
		data, err := models.GetCodes(request)
		if err != nil {
			log.Println("1: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.JSON(http.StatusOK, data)
		}
		break

	case "2":
		data, err := models.GetDistrictsByCity(request.City)
		if err != nil {
			log.Println("2: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.JSON(http.StatusOK, data)
		}
		break
	case "2.1":
		data, err := models.GetSetting("site.terms")
		if err != nil {
			log.Println("2.1: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.JSON(http.StatusOK, gin.H{"text": data.Value})
		}
		break

	case "3":
		data, err := models.GetFree(request)
		if err != nil {
			log.Println("3: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.JSON(http.StatusOK, data)
		}
		break

	case "3.1":

		err := models.SendMessage(request)
		if err != nil {
			log.Println("3.1: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {

			c.Status(http.StatusOK)
		}
		break
	case "5":
		data, err := models.GetInfoByCity(request.City)
		if err != nil {
			log.Println("5: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.JSON(http.StatusOK, data)
		}
	case "9":
		code, dataID, token, err := models.AddRegistrationData(request)

		if err != nil {
			log.Println("9: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {

			if request.Image != "" {
				files := helpers.Base64ToImage(request.Image, request.Phone)

				if len(files) > 0 {
					models.DeleteFilebyDataID(dataID)
				}

				for _, file := range files {
					models.AddFile(dataID, file.UploadedName)
				}

			}

			c.JSON(http.StatusOK, gin.H{
				"code":  code.Code,
				"token": token,
			})
		}

		break
	case "9.1":
		data, dataID, err := models.UpdateRegistrationData(request)

		if err != nil {
			log.Println("9.1: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {

			if dataID > 0 && request.Image != "" {

				files := helpers.Base64ToImage(request.Image, request.Phone)

				if len(files) > 0 {
					models.DeleteFilebyDataID(data.ID)
				}

				for _, file := range files {
					models.AddFile(data.ID, file.UploadedName)
				}

			}

			c.Status(http.StatusOK)
		}

		break
	case "10":
		err := models.SetRegistrationDataCode1(request)
		if err != nil {
			log.Println("10: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.Status(http.StatusOK)
		}
		break
	case "10.1":
		err := models.SetRegistrationDataCode2(request)
		if err != nil {
			log.Println("10.1: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.Status(http.StatusOK)
		}
		break

	case "13.1":
		err := models.SetStatus(request, 1)
		if err != nil {
			log.Println("13.1: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.Status(http.StatusOK)
		}
		break
	case "13.2":
		err := models.SetStatus(request, 0)
		if err != nil {
			log.Println("13.2: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.Status(http.StatusOK)
		}
		break

	case "13.3":
		err := models.AddCoordinates(request)
		if err != nil {
			log.Println("13.3: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.Status(http.StatusOK)
		}
		break
	case "14":

		phoneDB, err := strconv.ParseUint(request.Phone, 10, 64)

		if err != nil {
			log.Println("14: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		data, err := models.GetRegistrationData(phoneDB)
		if err != nil {
			log.Println("14: " + err.Error())
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.JSON(http.StatusOK, gin.H{
				"data": data,
			})
		}
		break
	default:
		c.Status(http.StatusBadRequest)
	}

}
