package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

//Ping  ping pong
func Ping(c *gin.Context) {

	c.JSON(http.StatusOK, gin.H{"status": "Ok"})

}
