package controllers

import (
	"net/http"

	"bitbucket.org/evgeniy-pevtsov-dev/taxiapp/models"
	"github.com/gin-gonic/gin"
)

//Cron main cron request
func Cron(c *gin.Context) {

	models.ClearExpired()
	models.ClearExpiredCodes()
	models.ClearAPPLog()

	c.Status(http.StatusOK)
}
